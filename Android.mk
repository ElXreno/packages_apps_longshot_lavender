LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE            := privapp-permissions-com.android.screenshot.xml
LOCAL_MODULE_TAGS       := optional
LOCAL_MODULE_CLASS      := ETC
LOCAL_MODULE_PATH       := $(TARGET_OUT_ETC)/permissions
LOCAL_SRC_FILES         := $(LOCAL_MODULE)
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE            := hidden-api-com.android.screenshot.xml
LOCAL_MODULE_TAGS       := optional
LOCAL_MODULE_CLASS      := ETC
LOCAL_MODULE_PATH       := $(TARGET_OUT_ETC)/permissions
LOCAL_SRC_FILES         := $(LOCAL_MODULE)
include $(BUILD_PREBUILT)

include $(CLEAR_VARS)
LOCAL_MODULE            := Longshot
LOCAL_MODULE_TAGS       := optional
LOCAL_MODULE_CLASS      := APPS
LOCAL_SRC_FILES         := Longshot.apk
LOCAL_CERTIFICATE       := platform
LOCAL_DEX_PREOPT        := false
LOCAL_MODULE_SUFFIX     := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_REQUIRED_MODULES  := privapp-permissions-org.codeaurora.snapcam.xml hidden-api-com.android.screenshot.xml
LOCAL_MULTILIB          := both
include $(BUILD_PREBUILT)
